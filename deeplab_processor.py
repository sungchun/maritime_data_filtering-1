import sys
import os
import multiprocessing
from multiprocessing import Lock
import numpy as np
import mxnet as mx
import cv2
import time
import threading
from PIL import Image
from mxnet.gluon.data.vision import transforms
from matplotlib import pyplot as plt
import gluoncv
from gluoncv.data.transforms.presets.segmentation import test_transform
from gluoncv.utils.viz import get_color_pallete
from gluoncv import model_zoo, data, utils
from gluoncv.data.transforms.mask import fill



#============================================================================================#
# DeepLab Version 3 Segmentation Algorithm
#============================================================================================#
class DeepLabProcessor(object):
    '''
    Performs DeepLab Semantic Segmentation
    '''
    def __init__(self, use_gpu=True, gpu_idx=0):
        if use_gpu:
            self.ctx = [mx.gpu(1), mx.gpu(0)]
            print("[!] Initializing DeepLapV3 Segmentation Processor (GPU)...", end=" ")
        else:
            cpu_count = 4  # multiprocessing.cpu_count()
            self.ctx = [mx.cpu(i) for i in range(cpu_count)]
            print("[!] Initializing DeepLapV3 Segmentation Processor (CPU)...", end=" ")

        self.model = gluoncv.model_zoo.get_model('deeplab_resnet101_ade', pretrained=True, ctx=self.ctx)
        self.thread_lock = multiprocessing.Lock()
        self.gpu_idx = gpu_idx
        self.boat_indices = [76, 103]
        self.land_indices = [3, 4, 6, 9, 11, 13, 16, 17, 29, 34, 46, 52, 54, 68, 72, 91, 94, 140]
        self.building_indices = [0, 1, 14, 25, 32, 48, 53, 58, 84]
        self.person_idx = [12]
        self.sky_idx = [2]
        self.water_indices = [21, 26, 60, 128]
        self.all = self.boat_indices + self.land_indices + self.building_indices + self.person_idx

        print("DONE!")

    def get_ratios(self, predictions, total_pixels):
        boat_pixels = np.sum(np.where(np.isin(predictions, self.boat_indices), 1, 0))
        land_pixels = np.sum(np.where(np.isin(predictions, self.land_indices), 1, 0))
        building_pixels = np.sum(np.where(np.isin(predictions, self.building_indices), 1, 0))
        person_pixels = np.sum(np.where(np.isin(predictions, self.person_idx), 1, 0))
        water_pixels = np.sum(np.where(np.isin(predictions, self.water_indices), 1, 0))
        sky_pixels = np.sum(np.where(np.isin(predictions, self.sky_idx), 1, 0))

        boat_ratio = (boat_pixels / total_pixels) * 100
        land_ratio = (land_pixels / total_pixels) * 100
        building_ratio = (building_pixels / total_pixels) * 100
        person_ratio = (person_pixels / total_pixels) * 100
        water_ratio = (water_pixels / total_pixels) * 100
        sky_ratio = (sky_pixels / total_pixels) * 100

        return boat_ratio, land_ratio, building_ratio, person_ratio, water_ratio, sky_ratio

    def get_output_mask(self, segment_dict, bg_img=None):
        if bg_img is not None and bg_img.any():
            mask_img = cv2.cvtColor(np.array(segment_dict["mask"].convert("RGB")), cv2.COLOR_RGB2BGR)
            dl_output_img = cv2.addWeighted(bg_img, 0.7, mask_img, 0.3, 0)
            
        else:
            dl_output_img = cv2.cvtColor(np.array(segment_dict["mask"].convert("RGB")), cv2.COLOR_RGB2BGR)
            
        info_str = "Boats: {:.2f}%\nLand: {:.2f}% \nBuildings: {:.2f}%\nPeople: {:.2f}%\nWater: {:.2f}%\nSky: {:.2f}%".format( \
            segment_dict["boat"], \
            segment_dict["land"], \
            segment_dict["building"], \
            segment_dict["person"], \
            segment_dict["water"], \
            segment_dict["sky"])            
            
        font = cv2.FONT_HERSHEY_SIMPLEX
        fontScale = 0.65
        color = (255, 255, 255)
        thickness = 2
        y0, dy = 35, 30
        for i, line in enumerate(info_str.split('\n')):
            y = y0 + (i * dy)
            dl_output_img = cv2.putText(dl_output_img, line, (20, y), font, fontScale, color, thickness, cv2.LINE_AA)

        return dl_output_img


    def process(self, img, collapse_classes=False):
        # save original
        orig_img = img.copy()

        # normalize the image using dataset mean
        img = test_transform(orig_img, self.ctx[self.gpu_idx])
        img = img.as_in_context(self.ctx[self.gpu_idx])

        # make prediction using single scale
        self.thread_lock.acquire()
        output = self.model.predict(img)
        self.thread_lock.release()
        predict = mx.nd.squeeze(mx.nd.argmax(output, 1)).asnumpy()

        # Get pixel ratios
        total_pixels = orig_img.shape[0] * orig_img.shape[1]
        boat_ratio, land_ratio, building_ratio, person_ratio, water_ratio, sky_ratio = self.get_ratios(predict, total_pixels)
        info_dict = {}
        info_dict["boat"] = boat_ratio
        info_dict["land"] = land_ratio
        info_dict["building"] = building_ratio
        info_dict["person"] = person_ratio
        info_dict["water"] = water_ratio
        info_dict["sky"] = sky_ratio

        # Get color pallette
        mask = get_color_pallete(predict, 'ade20k')
        info_dict["mask"] = mask

        # Get boat pixel mask
        info_dict["boat_mask"] = np.where(np.isin(predict, self.boat_indices), 1, predict)

        # Collapse classes
        if collapse_classes:
            predict = np.where(np.isin(predict, self.boat_indices), 76, predict)
            predict = np.where(np.isin(predict, self.land_indices), 94, predict)
            predict = np.where(np.isin(predict, self.building_indices), 1, predict)
            predict = np.where(np.isin(predict, self.water_indices), 21, predict)
            predict = np.where(np.isin(predict, self.all), predict, -1)            
        
        info_dict["predictions"] = predict

        return info_dict