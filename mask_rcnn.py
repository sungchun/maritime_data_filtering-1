import numpy as np
import mxnet as mx
import cv2
import time
import threading
import multiprocessing
from PIL import Image
from mxnet.gluon.data.vision import transforms
import gluoncv
from gluoncv.data.transforms.presets.segmentation import test_transform
from gluoncv.utils.viz import get_color_pallete
from gluoncv import model_zoo, data, utils
from gluoncv.data.transforms.mask import fill
from tqdm import tqdm


#============================================================================================#
# Mask R-CNN Object
#============================================================================================#
class MaskRCNN(object):
    '''
    Performs Mask R-CNN Instance segmentation
    '''
    def __init__(self, use_gpu=True, gpu_idx=0):
        if use_gpu:
            self.ctx = [mx.gpu(0), mx.gpu(1)]
            print("[!] Initializing MaskRCNN Detector (GPU)...", end=" ")
        else:
            cpu_count = 4
            self.ctx = [mx.cpu(i) for i in range(cpu_count)]
            print("[!] Initializing MaskRCNN Detector (CPU)...", end=" ")

        self.net = model_zoo.get_model('mask_rcnn_resnet50_v1b_coco', pretrained=True, ctx=self.ctx)
        self.gpu_idx = gpu_idx
        self.thread_lock = multiprocessing.Lock()
        print("DONE!")


    def expand_masks(self, masks, bboxes, ids, im_shape, scores=None, thresh=0.5, scale=1.0):
        if len(masks) != len(bboxes):
            raise ValueError('The length of bboxes and masks mismatch, {} vs {}'.format(
                len(bboxes), len(masks)))

        if scores is not None and len(masks) != len(scores):
            raise ValueError('The length of scores and masks mismatch, {} vs {}'.format(
                len(scores), len(masks)))

        if isinstance(masks, mx.nd.NDArray):
            masks = masks.asnumpy()

        if isinstance(bboxes, mx.nd.NDArray):
            bboxes = bboxes.asnumpy()

        if isinstance(ids, mx.nd.NDArray):
            ids = ids.asnumpy()

        if isinstance(scores, mx.nd.NDArray):
            scores = scores.asnumpy()

        sorted_inds = np.argsort(range(len(masks)))

        bboxes *= scale

        # First condition: pass detection threshold
        valid = np.where(scores >= thresh)[0]
        sorted_inds = sorted_inds[valid]
        masks = masks[valid]
        bboxes = bboxes[valid]
        ids = ids[valid]
        scores = scores[valid]

        full_masks = fill(masks, bboxes, im_shape)

        return full_masks, bboxes, scores, ids, sorted_inds


    def expand_and_filter_masks(self, masks, bboxes, ids, im_shape, scores=None, class_id=8, thresh=0.5, scale=1.0):
        if len(masks) != len(bboxes):
            raise ValueError('The length of bboxes and masks mismatch, {} vs {}'.format(
                len(bboxes), len(masks)))

        if scores is not None and len(masks) != len(scores):
            raise ValueError('The length of scores and masks mismatch, {} vs {}'.format(
                len(scores), len(masks)))

        if isinstance(masks, mx.nd.NDArray):
            masks = masks.asnumpy()

        if isinstance(bboxes, mx.nd.NDArray):
            bboxes = bboxes.asnumpy()

        if isinstance(ids, mx.nd.NDArray):
            ids = ids.asnumpy()

        if isinstance(scores, mx.nd.NDArray):
            scores = scores.asnumpy()

        sorted_inds = np.argsort(range(len(masks)))

        bboxes *= scale

        # First condition: pass detection threshold
        valid = np.where(scores >= thresh)[0]
        sorted_inds = sorted_inds[valid]
        masks = masks[valid]
        bboxes = bboxes[valid]
        ids = ids[valid]
        scores = scores[valid]

        # Second condition: filter by class
        valid = np.where(ids == class_id)[0]
        sorted_inds = sorted_inds[valid]
        masks = masks[valid]
        bboxes = bboxes[valid]
        ids = ids[valid]
        scores = scores[valid]

        full_masks = fill(masks, bboxes, im_shape)

        return full_masks, bboxes, scores, ids, sorted_inds

    def process(self, img):
        img, orig_img = data.transforms.presets.rcnn.transform_test(img)
                
        self.thread_lock.acquire()
        img = img.as_in_context(self.ctx[self.gpu_idx])
        ids, scores, bboxes, masks = [result[0].asnumpy() for result in self.net(img)]
        self.thread_lock.release()

        width, height = orig_img.shape[1], orig_img.shape[0]
        masks, _ = utils.viz.expand_mask(masks, bboxes, (width, height), scores)

        return ids, scores, bboxes, masks

    def get_mask(self, img, img_shape, adjust_boxes=True):
        img, orig_img = data.transforms.presets.rcnn.transform_test(img)
        
        self.thread_lock.acquire()
        img = img.as_in_context(self.ctx[self.gpu_idx])
        ids, scores, bboxes, masks = [result[0].asnumpy() for result in self.net(img)]
        self.thread_lock.release()

        width, height = orig_img.shape[1], orig_img.shape[0]
        masks, bboxes, scores, ids, _ = self.expand_masks(masks, bboxes, ids, (width, height), scores, thresh=0.75)
        boat_mask = np.zeros((height, width))
        person_mask = np.zeros((height, width))

        output_img = utils.viz.plot_mask(orig_img, masks, alpha=0.75)
        output_img = utils.viz.cv_plot_bbox(output_img, bboxes, scores, ids, class_names=self.net.classes)
        resized = cv2.resize(np.array(output_img), (img_shape[0], img_shape[1]), interpolation=cv2.INTER_AREA)

        boat_indices = np.where(ids == 8)[0]
        person_indices = np.where(ids == 0)[0]
        
        for m in masks[boat_indices]:
            boat_mask = np.where(boat_mask == 1, boat_mask, m)

        for m in masks[person_indices]:
            person_mask = np.where(person_mask == 1, person_mask, m)

        boat_mask = cv2.resize(boat_mask, (img_shape[0], img_shape[1]), interpolation=cv2.INTER_NEAREST)
        person_mask = cv2.resize(person_mask, (img_shape[0], img_shape[1]), interpolation=cv2.INTER_NEAREST)

        if adjust_boxes == True:
            bboxes[:, (0, 2)] /= width
            bboxes[:, (1, 3)] /= height
            bboxes[:, (0, 2)] *= img_shape[0]
            bboxes[:, (1, 3)] *= img_shape[1]

        segments = {}

        for idx, id in enumerate(ids):
            key = int(id[0])
            if key in segments:
                segments[key] += [(bboxes[idx], masks[idx], scores[idx])]
            else:
                segments[key] = [(bboxes[idx], masks[idx], scores[idx])]

        #return filtered_mask, bboxes, scores, resized, ids
        
        segments["person_mask"] = person_mask
        segments["boat_mask"] = boat_mask
        segments["output"] = resized
        
        return segments
