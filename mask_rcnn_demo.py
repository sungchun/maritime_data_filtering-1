#!/usr/bin/python3

import os, sys
import numpy as np
import cv2
import mxnet as mx
from PIL import Image
import gluoncv
from gluoncv import model_zoo, data, utils
from gluoncv.data.transforms.mask import fill
from tqdm import tqdm
import time


#
# Mask R-CNN Object
#
class MaskRCNN(object):
    def __init__(self, use_gpu = True):
        if use_gpu:
            self.ctx = [mx.gpu(0), mx.gpu(1)]
            print("[!] Initializing MaskRCNN Detector (GPU)...", end=" ")
        else:
            cpu_count = 4 # multiprocessing.cpu_count()
            self.ctx = [mx.cpu(i) for i in range(cpu_count)]
            print("[!] Initializing MaskRCNN Detector (CPU)...", end=" ")
            
        self.net = model_zoo.get_model('mask_rcnn_resnet50_v1b_coco', pretrained=True, ctx=self.ctx)
        print("DONE!")
        
    def expand_and_filter_masks(self, masks, bboxes, ids, im_shape, scores=None, class_id=8, thresh=0.5, scale=1.0):
        if len(masks) != len(bboxes):
            raise ValueError('The length of bboxes and masks mismatch, {} vs {}'.format(len(bboxes), len(masks)))
            
        if scores is not None and len(masks) != len(scores):
            raise ValueError('The length of scores and masks mismatch, {} vs {}'.format(len(scores), len(masks)))

        if isinstance(masks, mx.nd.NDArray):
            masks = masks.asnumpy()
            
        if isinstance(bboxes, mx.nd.NDArray):
            bboxes = bboxes.asnumpy()
            
        if isinstance(ids, mx.nd.NDArray):
            ids = ids.asnumpy()
            
        if isinstance(scores, mx.nd.NDArray):
            scores = scores.asnumpy()

        sorted_inds = np.argsort(range(len(masks)))

        bboxes *= scale
        
        # First condition: pass detection threshold
        valid = np.where(scores >= thresh)[0]
        sorted_inds = sorted_inds[valid]
        masks = masks[valid]
        bboxes = bboxes[valid]
        ids = ids[valid]
        scores = scores[valid]
        
        # Second condition: filter by class 
        valid = np.where(ids == class_id)[0]
        sorted_inds = sorted_inds[valid]
        masks = masks[valid]
        bboxes = bboxes[valid]
        ids = ids[valid]
        scores = scores[valid]
        
        full_masks = fill(masks, bboxes, im_shape)
        
        return full_masks, bboxes, scores, ids, sorted_inds        
        
    def process(self, img):
        # Please beware that `orig_img` is resized to short edge 600px.
        img_copy = img.copy()
        orig_width, orig_height = img.shape[1], img.shape[0]
        img = mx.nd.array(img)                            
        img, orig_img = data.transforms.presets.rcnn.transform_test(img)
        img = img.as_in_context(self.ctx[0])
        
        ids, scores, bboxes, masks = [result[0].asnumpy() for result in self.net(img)]     
        width, height = orig_img.shape[1], orig_img.shape[0]
        masks, _ = utils.viz.expand_mask(masks, bboxes, (width, height), scores)
        
        output_img = utils.viz.plot_mask(orig_img, masks, alpha=1.0)
        output_img = gluoncv.utils.viz.cv_plot_bbox(output_img, bboxes, scores, ids, class_names = self.net.classes)
        resized = cv2.resize(np.array(output_img), (orig_width, orig_height), interpolation = cv2.INTER_AREA)
        large_img = cv2.hconcat([img_copy, resized])
        
        return large_img
        
    def get_class_mask(self, img, cls_id=8):
        # Please beware that `orig_img` is resized to short edge 600px.
        img_copy = img.copy()
        orig_width, orig_height = img.shape[1], img.shape[0]
        img = mx.nd.array(img)                            
        img, orig_img = data.transforms.presets.rcnn.transform_test(img)
        img = img.as_in_context(self.ctx[0])
        
        ids, scores, bboxes, masks = [result[0].asnumpy() for result in self.net(img)]     
        width, height = orig_img.shape[1], orig_img.shape[0]
        masks, bboxes, scores, ids, _ = self.expand_and_filter_masks(masks, bboxes, ids, (width, height), scores, class_id=cls_id, thresh=0.75)
        filtered_mask = np.zeros((height, width))
        
        for cnt, m in enumerate(masks):
           filtered_mask = np.where(filtered_mask == 1, filtered_mask, m)
           
        filtered_mask = cv2.resize(filtered_mask, (orig_width, orig_height), interpolation = cv2.INTER_AREA)
        return filtered_mask
           

# Create MaskRCNN object
rcnn = MaskRCNN()

if __name__ == '__main__':
    if len(sys.argv) == 2:
        list_file = sys.argv[1]
        
        # Read list of images from file
        with open(list_file) as fp:
            print()
            #for cnt, fn in enumerate(tqdm(fp.read().splitlines())):
            for cnt, fn in enumerate(fp.read().splitlines()):
                if cnt <= 2000:
                    img = cv2.imread(fn)
                    
                    if img is not None and img.any():
                        large_img = rcnn.process(img)      
                        file_path = os.path.basename(fn)
                        output_path = "results/{}_masks.png".format(os.path.splitext(file_path)[0])
                        cv2.imwrite(output_path, large_img)
                    
            print("[!] Processed {} images.".format(cnt + 1))
    else:
        print("[!] Invalid command arguments provided")
        
