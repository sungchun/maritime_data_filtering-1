#!/usr/bin/python3
"""
    Boat Data Cleaning Procedures
"""
import sys
import os
from os import listdir
import numpy as np
import cv2
import time
import logging
import datetime
import gc
import argparse
import queue
    

# Miscellaneous
datetime_str = datetime.datetime.now().strftime('%Y%m%d-%H%M%S')
results_root = "results_{}".format(datetime_str)

# Argument Parser
parser = argparse.ArgumentParser(description="Tool to view the results of maritime image filtering")
parser.add_argument("FILTERING_RESULTS_FOLDER", help="Folder containing the accept folder which contains a list of text files. Example: results_20200625-095550/")
parser.add_argument('-v', action="store_false", dest='verbose', default=False, help="Output log messages for every image")
parser.add_argument('-output', action="store", dest="output_folder", default="results_{}".format(datetime_str), help="The destination folder for image results")


#
# Bounding Box clamping procedure
#
def clamp_bbox(xmin, ymin, xmax, ymax, img_shape):
    if xmin < 0:
        xmin = 0

    if ymin < 0:
        ymin = 0

    if xmax > (img_shape[1] - 1):
        xmax = img_shape[1] - 1

    if ymax > (img_shape[0] - 1):
        ymax = img_shape[0] - 1

    return xmin, ymin, xmax, ymax

#
# Class to traverse a list of files
#
class ListWalker(object):
    def __init__(self, text_files):
        self.curr_pos = 0
        self.prev_pos = 0
        self.next_pos = self.curr_pos + 1
        self.list_length = len(text_files)
        self.file_list = text_files

    def forward(self):
        # TODO: Evaluate
        item = self.file_list[self.curr_pos]
        self.curr_pos = self.next_pos
        self.prev_pos = self.curr_pos - 1 if (self.curr_pos - 1) >= 0 else 0
        self.next_pos = self.curr_pos + 1 if (self.curr_pos + 1) < self.list_length else (self.list_length - 1)
        return item

    def backward(self):
        # TODO: Evaluate
        item = self.file_list[self.curr_pos]
        self.curr_pos = self.prev_pos
        self.prev_pos = self.curr_pos - 1 if (self.curr_pos - 1) >= 0 else 0
        self.next_pos = self.curr_pos + 1 if (self.curr_pos + 1) < self.list_length else (self.list_length - 1)
        return item


#
# Function to display an image and its bounding box result post-filtering
#
def display_boat_image(file_list):
    walker = ListWalker(file_list)
    output_folder = "image_results"

    if not os.path.exists(output_folder):
        os.mkdir(output_folder)

    MAX_FILES = 100
    while walker.next_pos < walker.list_length and walker.next_pos < MAX_FILES:
        text_file = walker.forward()
        print(f"Working on file: {text_file}")

        if os.path.exists(text_file):
            with open(text_file) as fp:
                line = fp.readline()
                values = line.split(",")
                values = [v.strip() for v in values]
                image_name = values[0]
                xmin = float(values[1])
                ymin = float(values[2])
                box_w = float(values[3])
                box_h = float(values[4])
                xmax = xmin + box_w
                ymax = ymin + box_h
                
                img = cv2.imread(image_name)
                xmin, ymin, boxw, ymax = clamp_bbox(xmin, ymin, xmax, ymax, img.shape)
                            
                # Draw bounding box
                cv2.rectangle(img, (int(xmin), int(ymin)), (int(xmax), int(ymax)), (0, 255, 0), 3)
                file_path = os.path.basename(image_name)

                cv2.imwrite(f"{output_folder}/{file_path}.output.jpg", img)
                #cv2.imshow('Acceptable Boat Image', img)
                #cv2.waitKey(0)
                #cv2.destroyAllWindows()         

        else:
            print(f"[!] The text file {text_file} does not exist")
            

    return 0
        

def main(results_folder):
    accept_folder = os.path.join(results_folder, "accept")

    if os.path.exists(accept_folder) and os.path.isdir(accept_folder):
        all_files = [f for f in os.listdir(accept_folder) if os.path.isfile(os.path.join(accept_folder, f)) and f.lower().endswith('.txt')]
        all_files = [os.path.join(accept_folder, f) for f in all_files]
        print(f"[*] Found {len(all_files)} result files")

        return display_boat_image(all_files)

    else:
        print(f"[!] {accept_folder} is not a valid folder")
        return -1


#-------------#
# Entry Point #
#-------------#
if __name__ == '__main__':
    gc.enable()
    args = parser.parse_args()

    main(args.FILTERING_RESULTS_FOLDER)
    
    